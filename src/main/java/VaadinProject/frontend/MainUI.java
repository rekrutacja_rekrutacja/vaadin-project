package VaadinProject.frontend;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

import VaadinProject.frontend.views.LoginView;
import VaadinProject.frontend.views.NotebookListView;
import VaadinProject.frontend.views.RegistrationWizardView;

@SuppressWarnings("serial")
@Title("Notebook warehouse helper")
@Theme("mytheme")
@Push
public class MainUI extends UI {

	@Override
	protected void init(VaadinRequest request) {

		new Navigator(this, this);
		getNavigator().addView(NotebookListView.NAME, NotebookListView.class);
		getNavigator().addView(LoginView.NAME, LoginView.class);
		getNavigator().addView(RegistrationWizardView.NAME, RegistrationWizardView.class);
		getNavigator().navigateTo(NotebookListView.NAME);

		getNavigator().addViewChangeListener(new ViewChangeListener() {

			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {

				boolean isLoggedIn = (getSession() != null && getSession().getAttribute("username") != null);
				boolean isLoginView = event.getNewView() instanceof LoginView
						|| event.getNewView() instanceof RegistrationWizardView;

				if (isLoggedIn && isLoginView) {
					getNavigator().navigateTo(NotebookListView.NAME);
					Notification.show("Nice try :)", Type.HUMANIZED_MESSAGE);
					return false;
				}

				return true;
			}

			@Override
			public void afterViewChange(ViewChangeEvent event) {

			}
		});
	}

	@WebServlet(urlPatterns = "/*", asyncSupported=true)
	@VaadinServletConfiguration(ui = MainUI.class, productionMode = false )
	public static class MainUIServlet extends VaadinServlet {
	}
}
