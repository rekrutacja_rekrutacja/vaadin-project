package VaadinProject.frontend.registrationSteps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;

import VaadinProject.frontend.views.RegistrationWizardView;

@SuppressWarnings("serial")
public class RegistrationStep5 implements WizardStep {

	private Image avatar = new Image();
	private boolean avatarSet = false;

	public static final String fieldWidth = "350px";

	@Override
	public String getCaption() {
		return "Upload your avatar";
	}

	@Override
	public Component getContent() {

		class ImageUploader implements Receiver, SucceededListener {
			public File file;

			@Override
			public OutputStream receiveUpload(String filename, String mimeType) {
				FileOutputStream fos = null;
				try {
					file = new File("C:/WINDOWS/Temp/" + filename);
					fos = new FileOutputStream(file);
				} catch (final java.io.FileNotFoundException e) {
					new Notification("Could not open file<br/>", e.getMessage(), Notification.Type.ERROR_MESSAGE)
							.show(Page.getCurrent());
					return null;
				}
				return fos;
			}

			@Override
			public void uploadSucceeded(SucceededEvent event) {
				if (file.length() > 200 * 1024) {
					Notification.show("File rejected. Max 200kb files are accepted.", Type.WARNING_MESSAGE);
					return;
				}

				avatar.setVisible(true);
				avatar.setSource(new FileResource(file));
				avatarSet = true;
			}
		};

		avatar.setStyleName("avatar");
		avatar.setWidth("300px");
		avatar.setHeight("300px");

		ImageUploader receiver = new ImageUploader();

		Upload upload = new Upload(null, receiver);
		upload.setImmediate(true);
		upload.setButtonCaption("Upload your avatar");
		upload.addSucceededListener(receiver);

		VerticalLayout content = new VerticalLayout();

		Label text = getText();
		content.addComponent(text);

		VerticalLayout fields = new VerticalLayout(avatar, upload);
		fields.setSpacing(true);
		fields.setMargin(true);
		fields.setComponentAlignment(avatar, Alignment.MIDDLE_CENTER);
		fields.setComponentAlignment(upload, Alignment.MIDDLE_CENTER);

		content.addComponent(fields);

		return content;
	}

	private Label getText() {
		return new Label(
				"<h1>Upload your avatar</h1>" + "<p><h2><i>Max size of your avatar is <b>200kb</b>.</i></h2></p>",
				ContentMode.HTML);
	}

	public boolean onAdvance() {
		if (!avatarSet) {
			Notification.show("Upload your avatar", Type.ERROR_MESSAGE);
			return false;
		}

		RegistrationWizardView.newUser.setAvatar(avatar);
		return true;
	}

	public boolean onBack() {
		return true;
	}
}
