package VaadinProject.frontend.registrationSteps;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.data.Validator;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.helpers.PasswordHelper;
import VaadinProject.backend.validators.UserValidator;
import VaadinProject.frontend.views.RegistrationWizardView;

@SuppressWarnings("serial")
public class RegistrationStep4 implements WizardStep {

	private PasswordField password1;
	private PasswordField password2;
	private Label errorLabel;

	public static final String fieldWidth = "350px";

	@Override
	public String getCaption() {
		return "Enter your password";
	}

	@Override
	public Component getContent() {

		password1 = new PasswordField("Password:");
		password1.setWidth(fieldWidth);
		password1.setRequired(true);
		password1.addValidator(UserValidator.PasswordValidator);
		password1.focus();

		password2 = new PasswordField("Repeat password:");
		password2.setWidth(fieldWidth);
		password2.setRequired(true);
		password2.addValidator(new Validator() {

			@Override
			public void validate(Object value) throws InvalidValueException {
				String p1 = password1.getValue();
				String p2 = password2.getValue();
				if ((p1 == null || p2 == null) || (!p1.equals(p2))) {
					throw new InvalidValueException("Passwords do not match");
				}
			}
		});

		errorLabel = new Label("Password cannot contains username");
		errorLabel.setStyleName(ValoTheme.LABEL_FAILURE);
		errorLabel.setWidth(fieldWidth);
		errorLabel.setVisible(false);

		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);

		Label text = getText();
		content.addComponent(text);

		VerticalLayout fields = new VerticalLayout(password1, password2, errorLabel);
		fields.setSpacing(true);
		fields.setSizeUndefined();

		content.addComponent(fields);
		content.setComponentAlignment(fields, Alignment.TOP_CENTER);

		return content;
	}

	private Label getText() {
		return new Label("<h1>Enter your password</h1>"
				+ "<p><h2><i>Your Password must be <b>at least 8 characters</b> including at least <b>one number</b>, "
				+ "<b>one uppercase</b> and <b>one lowercase</b>.</i></h2></p>", ContentMode.HTML);
	}

	private boolean fieldsValid() {
		String username = RegistrationWizardView.newUser.getName().toLowerCase();
		String password = password1.getValue().toLowerCase();
		if (password.contains(username)) {
			return false;
		}
		return true;
	}

	public boolean onAdvance() {
		if (password1.getValue() == null || password1.getValue() == "") {
			Notification.show("Enter your password", Type.ERROR_MESSAGE);
			return false;
		}
		if (!password1.isValid() || !password2.isValid()) {
			return false;
		}
		if (!fieldsValid()) {
			errorLabel.setVisible(true);
			return false;
		}

		RegistrationWizardView.newUser.setPassword(PasswordHelper.computeHash(password1.getValue()));
		return true;
	}

	public boolean onBack() {
		return true;
	}
}
