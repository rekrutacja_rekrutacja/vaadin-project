package VaadinProject.frontend.registrationSteps;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class RegistrationStep1 implements WizardStep {

	@Override
	public String getCaption() {
		return "Registration wizard";
	}

	@Override
	public Component getContent() {
		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);

		Label text = getText();
		content.addComponent(text);

		return content;
	}

	private Label getText() {
		return new Label("<h1>Welcome in user registration!</h1>"
				+ "<p><h2>To continue registration please click the <b>next</b> button.</h2></p>", ContentMode.HTML);
	}

	public boolean onAdvance() {
		return true;
	}

	public boolean onBack() {
		return true;
	}

}
