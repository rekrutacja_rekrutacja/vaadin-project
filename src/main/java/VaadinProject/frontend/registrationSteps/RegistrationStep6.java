package VaadinProject.frontend.registrationSteps;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.frontend.views.NotebookListView;

public class RegistrationStep6 implements WizardStep {
	
	private static final Label TERMS = new Label(
			"<h2>Terms of use</h2>"
			+ "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut "
			+ "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris "
			+ "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit "
			+ "esse cillum dolore eu fugiat nulla pariatur.</p><br>"
			+ "<ul><li>Excepteur sint occaecat cupidatat non proident</li>"
			+ "<li>sunt in culpa qui officia deserunt mollit anim id est laborum</li></ul><br>",
	ContentMode.HTML);

	private CheckBox checkbox;
	private Window subWindow;
	
	private boolean userClick = true;

	@Override
	public String getCaption() {
		return "Terms of use";
	}

	@Override
	public Component getContent() {
		Label text = getText();

		checkbox = new CheckBox("I accept terms of use");
		checkbox.setStyleName(ValoTheme.CHECKBOX_LARGE);
		checkbox.addValueChangeListener(e -> {
			if (checkbox.getValue() && userClick) {
				checkbox.setValue(false);
				showTermsSubwindow();
			}
		});
		
		VerticalLayout content = new VerticalLayout(text, checkbox);
		content.setSizeFull();
		content.setMargin(true);

		content.setComponentAlignment(checkbox, Alignment.TOP_CENTER);

		return content;
	}

	private void showTermsSubwindow() {
		subWindow = new Window();
		VerticalLayout subContent = new VerticalLayout();
		subContent.setMargin(true);
		
		subWindow.setWidth("800px");
		subContent.addComponent(TERMS);
		subWindow.center();
		
		Button acceptButon = new Button("Accept", e -> {
			subWindow.close();
			userClick = false;
			checkbox.setValue(true);
			userClick = true;
		});
		acceptButon.setStyleName(ValoTheme.BUTTON_PRIMARY);
		
		Button refuseButon = new Button("Refuse", e -> {
			subWindow.close();
			UI.getCurrent().getNavigator().navigateTo(NotebookListView.NAME);
			Notification.show("Registration cancelled", Type.WARNING_MESSAGE);
		});
		refuseButon.setStyleName(ValoTheme.BUTTON_DANGER);
		
		HorizontalLayout buttons = new HorizontalLayout(refuseButon, acceptButon);
		buttons.setSpacing(true);
		
		subContent.addComponent(buttons);
		subContent.setComponentAlignment(buttons, Alignment.TOP_CENTER);
		
		subWindow.setContent(subContent);
		UI.getCurrent().addWindow(subWindow);
	}

	private Label getText() {
		return new Label(
				"<h1>Terms of use</h1><p><h2>In order to complete your registration you must accept the terms of use</h2></p>",
				ContentMode.HTML);
	}

	public boolean onAdvance() {
		if (checkbox.getValue()) {
			return true;
		} else {

			Notification.show("You must accept terms of use", Type.HUMANIZED_MESSAGE);
			return false;
		}
	}

	public boolean onBack() {
		return true;
	}

}
