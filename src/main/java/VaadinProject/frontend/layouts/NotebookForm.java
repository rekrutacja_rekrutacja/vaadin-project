package VaadinProject.frontend.layouts;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.entities.Notebook;
import VaadinProject.backend.entities.User;
import VaadinProject.backend.validators.NotebookValidator;
import VaadinProject.frontend.views.NotebookListView;

@SuppressWarnings("serial")
public class NotebookForm extends FormLayout {

	Button save = new Button("Save", this::save);
	Button cancel = new Button("Cancel", this::cancel);
	Button delete = new Button("Delete", this::delete);

	TextField producer = new TextField("Producer");
	TextField model = new TextField("Model");
	TextField dimension = new TextField("Dimension");
	TextField resolution = new TextField("Resolution");
	TextField storage = new TextField("Storage");
	TextField memory = new TextField("Memory");
	TextField processor = new TextField("Processor");
	TextField graphicsCard = new TextField("Graphics Card");
	TextField os = new TextField("OS");
	TextField amount = new TextField("Amount");
	TextField price = new TextField("Price");

	Notebook notebook;

	BeanFieldGroup<Notebook> formFieldBindings;

	public NotebookForm() {
		configureComponents();
		buildLayout();
	}

	private void configureComponents() {
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		save.setIcon(FontAwesome.FLOPPY_O);

		cancel.setIcon(FontAwesome.BAN);
		cancel.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);

		delete.setStyleName(ValoTheme.BUTTON_DANGER);
		delete.setIcon(FontAwesome.TRASH_O);
		delete.setClickShortcut(ShortcutAction.KeyCode.DELETE);

		setVisible(false);
	}

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(save, cancel);
		actions.setSpacing(true);
		HorizontalLayout deleteAction = new HorizontalLayout(delete);

		addComponents(actions, producer, model, dimension, resolution, storage, memory, processor, graphicsCard, os,
				amount, price, deleteAction);
	}

	public void save(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			NotebookListView.service.save(notebook);

			String msg = String.format("'%s %s' saved", notebook.getProducer(), notebook.getModel());
			Notification.show(msg, Type.HUMANIZED_MESSAGE);
			NotebookListView.refreshNotebooks();
			setVisible(false);
		} catch (FieldGroup.CommitException e) {
			Notification.show("Invalid field(s)", Type.WARNING_MESSAGE);
		}
		
	}

	public void cancel(Button.ClickEvent event) {
		Notification.show("Cancelled", Type.HUMANIZED_MESSAGE);
		setVisible(false);
		NotebookListView.notebookList.select(null);
		for (Field<?> f : formFieldBindings.getFields()) {
			f.removeAllValidators();
		}
	}

	public void edit(Notebook notebook) {
		setElementsVisibility();
		this.notebook = notebook;
		if (notebook != null) {
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(notebook, this);
			
			formFieldBindings.getField("producer").addValidator(NotebookValidator.producerValidator);
			formFieldBindings.getField("processor").addValidator(NotebookValidator.processorValidator);
			formFieldBindings.getField("os").addValidator(NotebookValidator.osValidator);
			formFieldBindings.getField("graphicsCard").addValidator(NotebookValidator.graphicsCardValidator);
		}
		producer.focus();

		setVisible(notebook != null);
	}

	private void setElementsVisibility() {
		if (getSession() != null && getSession().getAttribute("role") != null) {
			if (getSession().getAttribute("role") == User.Role.ADMIN) {
				save.setVisible(true);
				delete.setVisible(true);
			} else {
				save.setVisible(true);
				delete.setVisible(false);
			}
		} else {
			save.setVisible(false);
			delete.setVisible(false);
		}
	}

	public void delete(Button.ClickEvent event) {
		Notebook notebook = (Notebook) NotebookListView.notebookList.getSelectedRow();
		if (notebook != this.notebook) {
			Notification.show("Cannot delete nonexistent notebook", Type.HUMANIZED_MESSAGE);
			return;
		}
		ConfirmDialog.show(getUI(), "Please Confirm:", "Are you sure?", "I am", "I am not", dialog -> {
			if (dialog.isConfirmed()) {
				NotebookListView.service.delete(notebook);
				String msg = String.format("'%s %s' deleted", notebook.getProducer(), notebook.getModel());
				Notification.show(msg, Type.HUMANIZED_MESSAGE);
				NotebookListView.refreshNotebooks();
			} else {
				Notification.show("Cancelled", Type.HUMANIZED_MESSAGE);
			}
		});
	}
}