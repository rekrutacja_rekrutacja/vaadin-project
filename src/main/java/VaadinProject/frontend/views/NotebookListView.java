package VaadinProject.frontend.views;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.entities.Notebook;
import VaadinProject.backend.entities.User;
import VaadinProject.backend.services.NotebookService;
import VaadinProject.frontend.layouts.Menu;
import VaadinProject.frontend.layouts.NotebookForm;


@SuppressWarnings("serial")
public class NotebookListView extends CustomComponent implements View {
	
	public static final String NAME = "NotebookList";
	
	Menu menu = new Menu();
	Button openMenuButton = new Button();
	static TextField filter = new TextField();
	public static Grid notebookList = new Grid();
	Button newNotebook = new Button("New notebook");

	NotebookForm notebookForm = new NotebookForm();
	public static NotebookService service = NotebookService.createMockService();
	
	public NotebookListView() {
		configureComponents();
		buildLayout();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		setElementsVisibility();
	}
	
	private void setElementsVisibility() {
		if (getSession() != null && getSession().getAttribute("role") == User.Role.ADMIN) {
			newNotebook.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			newNotebook.addClickListener(e -> notebookForm.edit(new Notebook()));
		} else {newNotebook.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			newNotebook.addClickListener(e -> Notification.show("Only admin can add new notebooks", Type.HUMANIZED_MESSAGE));
		}
	}

	private void configureComponents() {
		setSizeFull();
		
		openMenuButton.addClickListener( e -> menu.show());
		openMenuButton.setIcon(FontAwesome.BARS);
		openMenuButton.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		openMenuButton.addStyleName("menu_btn");
		openMenuButton.setDescription("Open menu");

		newNotebook.setIcon(FontAwesome.LAPTOP);

		filter.setInputPrompt("Filter notebooks...");
		filter.addTextChangeListener(e -> refreshNotebooks(e.getText()));

		notebookList.setContainerDataSource(new BeanItemContainer<>(Notebook.class));
		notebookList.setColumnOrder("producer", "model", "amount", "price");
		try {
			notebookList.removeColumn("id");
			notebookList.removeColumn("dimension");
			notebookList.removeColumn("resolution");
			notebookList.removeColumn("storage");
			notebookList.removeColumn("memory");
			notebookList.removeColumn("processor");
			notebookList.removeColumn("graphicsCard");
			notebookList.removeColumn("os");
		} catch (Exception e) {}
		notebookList.setSelectionMode(Grid.SelectionMode.SINGLE);
		notebookList.addSelectionListener(e -> notebookForm.edit((Notebook) notebookList.getSelectedRow()));
		refreshNotebooks();
	}

	private void buildLayout() {
		HorizontalLayout actions = new HorizontalLayout(openMenuButton, filter, newNotebook);
		actions.setWidth("100%");
		filter.setWidth("100%");
		actions.setExpandRatio(filter, 1);

		VerticalLayout list = new VerticalLayout(actions, notebookList);
		list.setSizeFull();
		notebookList.setSizeFull();
		list.setExpandRatio(notebookList, 1);

		notebookForm = new NotebookForm();
		HorizontalLayout mainLayout = new HorizontalLayout(list, notebookForm);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(list, 1);
		
		HorizontalLayout all = new HorizontalLayout(menu, mainLayout);
		all.setSizeFull();
		all.setExpandRatio(menu, 1);
		all.setExpandRatio(mainLayout, 7);
		
		setCompositionRoot(all);
	}	

	public static void refreshNotebooks() {
		refreshNotebooks(filter.getValue());
	}

	private static void refreshNotebooks(String stringFilter) {
		notebookList.setContainerDataSource(new BeanItemContainer<>(Notebook.class, service.findAll(stringFilter)));
	}
}
