package VaadinProject.backend.helpers;

import java.security.MessageDigest;

public class PasswordHelper {
	public static String computeHash(String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(password.getBytes());
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
			    if ((0xff & hash[i]) < 0x10) {
			        hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
			    } else {
			        hexString.append(Integer.toHexString(0xFF & hash[i]));
			    }
			}
		    return hexString.toString();
		} catch (Exception e) {}
		return "";
	}
}
