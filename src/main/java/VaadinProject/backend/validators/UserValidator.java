package VaadinProject.backend.validators;

import com.vaadin.data.validator.AbstractValidator;

import VaadinProject.backend.services.UserService;

@SuppressWarnings("serial")
public class UserValidator {

	public static AbstractValidator<String> UsernameValidator = new AbstractValidator<String>(
			"Username must be at least 3 characters") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null && value.length() < 3) {
				return false;
			}
			return true;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};
	
	public static AbstractValidator<String> UniqueUsernameValidator = new AbstractValidator<String>(
			"Username must be unique") {
		
		UserService userService = UserService.createMockService();

		@Override
		protected boolean isValidValue(String value) {
			return userService.isUsernameUnique(value);
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};
	
	public static AbstractValidator<String> UniqueEmailValidator = new AbstractValidator<String>(
			"Email must be unique") {
		
		UserService userService = UserService.createMockService();

		@Override
		protected boolean isValidValue(String value) {
			return userService.isEmailUnique(value);
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};
	
	public static AbstractValidator<String> PasswordValidator = new AbstractValidator<String>(
			"Password must be at least 8 characters including at least one number, one uppercase and one lowercase") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null && (value.length() < 8 || !value.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).+"))) {
				return false;
			}
			return true;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};
}
