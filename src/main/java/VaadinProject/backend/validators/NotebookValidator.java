package VaadinProject.backend.validators;

import com.vaadin.data.validator.AbstractValidator;

@SuppressWarnings("serial")
public class NotebookValidator {
	public static AbstractValidator<String> processorValidator = new AbstractValidator<String>(
			"must start with 'Intel' or 'AMD'") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null) {
				if (value.startsWith("Intel") || value.startsWith("AMD")) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};

	public static AbstractValidator<String> producerValidator = new AbstractValidator<String>(
			"must start with an uppercase") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null && !value.equals("")) {
				if (Character.isUpperCase(value.charAt(0))) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};

	public static AbstractValidator<String> osValidator = new AbstractValidator<String>(
			"must contain 'Windows' or 'OS X' or 'Linux'") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null) {
				if (value.contains("Windows") || value.contains("OS X") || value.contains("Linux")) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};

	public static AbstractValidator<String> graphicsCardValidator = new AbstractValidator<String>(
			"'integrated' or must start with 'Nvidia' or 'AMD'") {

		@Override
		protected boolean isValidValue(String value) {
			if (value != null) {
				if ("integrated".equals(value) || value.startsWith("Nvidia") || value.startsWith("AMD")) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
	};
}
