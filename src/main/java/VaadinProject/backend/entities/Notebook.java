package VaadinProject.backend.entities;

import javax.validation.constraints.Size;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.apache.commons.beanutils.BeanUtils;

public class Notebook {

	private Long id;
	@Size(min=2, max=20)
	private String producer;
	@Size(min=1, max=20)
	private String model;
	@Pattern(regexp="\\d+\\.?\\d", message="format: ([digit])[digit].[digit]")
	private String dimension;
	@Size(min=7, max=9)
	@Pattern(regexp="\\d+x\\d+", message="format: [horizontal]x[vertical]")
	private String resolution;
	@Size(min=4, max=6)
	@Pattern(regexp="\\d+ (G|T)B", message="format: [digits] GB/TB")
	private String storage;
	@Size(min=4, max=5)
	@Pattern(regexp="\\d+ GB", message="format: [digits] GB")
	private String memory;
	@Size(min=2, max=20)
	private String processor;
	private String graphicsCard;
	@Size(min=2, max=20)
	private String os;
	@Min(0)
	private int amount;
	@DecimalMin("199.00")
	private BigDecimal price;

//	public Notebook() {
//		producer = "N.E.W.";
//		model = "123";
//		dimension = "15.6";
//		resolution = "1234x123";
//		storage = "123 GB";
//		memory = "1 GB";
//		processor = "AMD";
//		graphicsCard = "AMD";
//		os = "OS X";
//		amount = 0;
//		price = new BigDecimal(500);
//	}
	
	public Notebook() {
		producer = "";
		model = "";
		dimension = "";
		resolution = "";
		storage = "";
		memory = "";
		processor = "";
		graphicsCard = "";
		os = "";
		amount = 0;
		price = new BigDecimal(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getGraphicsCard() {
		return graphicsCard;
	}

	public void setGraphicsCard(String graphics_card) {
		this.graphicsCard = graphics_card;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public Notebook clone() throws CloneNotSupportedException {
		try {
			return (Notebook) BeanUtils.cloneBean(this);
		} catch (Exception ex) {
			throw new CloneNotSupportedException();
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("id=");
		builder.append(id);
		builder.append(", producer=");
		builder.append(producer);
		builder.append(", model=");
		builder.append(model);
		builder.append(", dimension=");
		builder.append(dimension);
		builder.append(", resolution=");
		builder.append(resolution);
		builder.append(", storage=");
		builder.append(storage);
		builder.append(", memory=");
		builder.append(memory);
		builder.append(", processor=");
		builder.append(processor);
		builder.append(", graphics card=");
		builder.append(graphicsCard);
		builder.append(", os=");
		builder.append(os);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", price=");
		builder.append(price);
		return builder.toString();
	}
}
