package VaadinProject.backend.services;

import org.apache.commons.beanutils.BeanUtils;

import VaadinProject.backend.entities.Notebook;

import java.math.BigDecimal;
import java.util.*;

public class NotebookService {

	static String[] producers = { "Asus", "Sony", "Acer", "Lenovo", "MSI", "HP", "Dell" };
	static String[] models = { "ABC-123", "UX01PL02", "Z510", "R.A.T.", "QX765", "X99" };
	static String[] dimensions = { "11.6", "13.3", "14", "15.6", "17.3" };
	static String[] resolutions = { "1280x800", "1366x768", "1920x1080", "2560x1600" };
	static String[] storages = { "32 GB", "128 GB", "256 GB", "500 GB", "1 TB" };
	static String[] memories = { "2 GB", "4 GB", "8 GB", "12 GB", "16 GB" };
	static String[] processors = { "Intel i5-4200U", "Intel i7-6300HQ", "AMD A8-7610", "Intel Atom N5100" };
	static String[] graphicsCards = { "integrated", "Nvidia GT 760M", "Nvidia GT 970M", "AMD R7-390" };
	static String[] oses = { "Windows 7 HP", "Windows 8 Pro", "Windows 10", "OS X 10.11", "Manjaro Linux 16.03" };
	static int[] amounts = { 1, 5, 10, 20, 50 };
	static double[] prices = { 1099.99, 1499.99, 1799.99, 2199.99, 2999.99, 3499.99, 4099.99 };

	private static NotebookService instance;

	public static NotebookService createMockService() {
		if (instance == null) {

			final NotebookService notebookService = new NotebookService();

			Random r = new Random(0);
			for (int i = 0; i < 20; i++) {
				Notebook notebook = new Notebook();
				notebook.setProducer(producers[r.nextInt(producers.length)]);
				notebook.setModel(models[r.nextInt(models.length)]);
				notebook.setDimension(dimensions[r.nextInt(dimensions.length)]);
				notebook.setResolution(resolutions[r.nextInt(resolutions.length)]);
				notebook.setStorage(storages[r.nextInt(storages.length)]);
				notebook.setMemory(memories[r.nextInt(memories.length)]);
				notebook.setProcessor(processors[r.nextInt(processors.length)]);
				notebook.setGraphicsCard(graphicsCards[r.nextInt(graphicsCards.length)]);
				notebook.setOs(oses[r.nextInt(oses.length)]);
				notebook.setAmount(amounts[r.nextInt(amounts.length)]);
				notebook.setPrice(new BigDecimal(prices[r.nextInt(prices.length)]));
				notebookService.save(notebook);
			}
			instance = notebookService;
		}

		return instance;
	}

	private HashMap<Long, Notebook> notebooks = new HashMap<>();
	private long nextId = 0;

	public synchronized List<Notebook> findAll(String stringFilter) {
		ArrayList<Notebook> arrayList = new ArrayList<>();
		for (Notebook notebook : notebooks.values()) {
			try {
				boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
						|| notebook.toString().toLowerCase().contains(stringFilter.toLowerCase());
				if (passesFilter) {
					arrayList.add(notebook.clone());
				}
			} catch (CloneNotSupportedException ex) {
			}
		}
		Collections.sort(arrayList, (o1, o2) -> {
			String x = o1.getProducer();
            String y = o2.getProducer();
            int cmp = x.compareTo(y);

            if (cmp != 0) {
               return cmp;
            } else {
               String x2 = o1.getModel();
               String y2 = o2.getModel();
               int cmp2 = x2.compareTo(y2);
               if (cmp2 != 0) {
            	   return cmp2;
               } else {
            	   return (int) (o2.getId() - o1.getId());
               }
            }
		});
		return arrayList;
	}

	public synchronized long count() {
		return notebooks.size();
	}

	public synchronized void delete(Notebook value) {
		notebooks.remove(value.getId());
	}

	public synchronized void save(Notebook entry) {
		if (entry.getId() == null) {
			entry.setId(nextId++);
		}
		try {
			entry = (Notebook) BeanUtils.cloneBean(entry);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		notebooks.put(entry.getId(), entry);
	}
}