package VaadinProject.backend.services;

import java.util.HashMap;

import org.apache.commons.beanutils.BeanUtils;

import com.vaadin.server.ClassResource;
import com.vaadin.ui.Image;

import VaadinProject.backend.entities.User;
import VaadinProject.backend.helpers.PasswordHelper;

public class UserService {

	public static UserService instance;
	public HashMap<Long, User> users = new HashMap<>();
	private long nextId = 0;

	public static UserService createMockService() {
		if (instance == null) {

			UserService userService = new UserService();

			User admin = new User();
			admin.setRole(User.Role.ADMIN);
			admin.setName("admin");
			admin.setEmail("admin@host.com");
			admin.setPassword(PasswordHelper.computeHash("admin"));
			admin.setAvatar(new Image(null, new ClassResource("/admin.jpg")));
			userService.save(admin);

			User user = new User();
			user.setRole(User.Role.USER);
			user.setName("user");
			user.setEmail("user@host.com");
			user.setPassword(PasswordHelper.computeHash("123456"));
			user.setAvatar(new Image(null, new ClassResource("/user.jpg")));
			userService.save(user);

			instance = userService;
		}

		return instance;
	}

	public synchronized User find(String username, String password) {
		for (User user : users.values()) {
			if (user.getName().equals(username) || user.getEmail().equals(username)) {
				if (user.getPassword().equals(PasswordHelper.computeHash(password))) {
					return user;
				}
			}
		}
		return null;
	}
	
	public synchronized boolean isUsernameUnique(String username) {
		for (User user : users.values()) {
			if (user.getName().equals(username)) {
					return false;
			}
		}
		return true;
	}
	
	public boolean isEmailUnique(String email) {
		for (User user : users.values()) {
			if (user.getEmail().equals(email)) {
					return false;
			}
		}
		return true;
	}

	public synchronized void delete(User user) {
		users.remove(user.getId());
	}

	public synchronized void save(User user) {
		if (user.getId() == null) {
			user.setId(nextId++);
		}
		try {
			user = (User) BeanUtils.cloneBean(user);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		users.put(user.getId(), user);
	}
}